const Nightmare = require('nightmare')
var vo = require('vo')
const fs = require('fs')

const START_URL = 'https://www.booking.com/searchresults.html?label=gen173nr-1FCAEoggJCAlhYSDNYBGg6iAEBmAExuAEHyAEN2AEB6AEB-AECkgIBeagCAw&sid=e61411fbc26c2cb551b944b80e42e3cd&city=-542884&class_interval=1&dest_id=-542218&dest_type=city&dtdisc=0&from_sf=1&group_adults=2&group_children=0&inac=0&index_postcard=0&label_click=undef&no_rooms=1&oos_flag=0&postcard=0&raw_dest_type=city&room1=A%2CA&sb_price_type=total&search_selected=1&src=searchresults&ss=Broumov%2C%20Hradec%20Kralove%2C%20Czech%20Republic&ss_all=0&ss_raw=broumov&ssb=empty&sshis=0&ssne_untouched=České%20Budějovice&=&=&nflt=ht_id%3D201%3B&lsf=ht_id%7C201%7C9&percent_htype_apt=1&unchecked_filter=hoteltype'
const STORY_SELECTOR = '.sr-hotel__name'
const AUTOSUGGEST = '.c-autocomplete__item:first-child'
const IDENTIFICATOR = "Country"

var nightmare = Nightmare({ show: false,
  webPreferences: {
    images: false,
  }
})

function* runFirst() {
    var nightmare = Nightmare({ show: false, openDevTools: true }),
        MAX_PAGE = 1000,
        currentPage = 0,
        nextExists = true,
        links = [];

    yield nightmare
    .goto(START_URL)
    .wait(2000)

    nextExists = yield nightmare.visible('.paging-next');

    while (nextExists && currentPage < MAX_PAGE) {
        links = links.concat(yield nightmare
            .evaluate(function() {
                var linkArray = [];
                var links = document.querySelectorAll('.hotel_name_link');
                console.log('links',links);
                return [...links].map(link => link.href);
            }));

        yield nightmare
            .click('.paging-next')
            .wait(2000)

        currentPage++;
        nextExists = yield nightmare.visible('.paging-next');
    }
    links = links.concat(yield nightmare
        .evaluate(function() {
            var linkArray = [];
            var links = document.querySelectorAll('.hotel_name_link');
            console.log('links',links);
            return [...links].map(link => link.href);
        }));
    console.log('links',links);
    fs.writeFile('links.json', JSON.stringify(links, null, 4), function (err) {
        console.log('details saved to links.json!');
    });
    yield nightmare.end();
}

function* runSecond() {
  var urls = require('./links.json')
  var results = []
  for (var i = 0; i < urls.length; i++) {
    let r = yield nightmare.goto(urls[i])
      .evaluate(() => {
           let result = {
             title: window.booking.env.b_hotel_name,
             location: [window.booking.env.b_map_center_longitude,window.booking.env.b_map_center_latitude],
             country: 'IDENTIFICATOR'
           }
           console.log('result',result)
           return result
         })
        console.log('result with index',i,'out of',urls.length,':',r)
        results.push(r)
  }
  console.log('results',results)
  return results
}

vo(runFirst)(function(err) {
    if (err) throw err;
    vo(runSecond)((err, results) => {
        if (err) throw err;
        let finals = checkDuplicities(results).map(res => {
          res.state = IDENTIFICATOR
        })
        fs.writeFile('result.' + IDENTIFICATOR + '.json', JSON.stringify(results, null, 4), function (err) {
            console.log('details saved to','result.' + IDENTIFICATOR + '.json');
        });
    });
});

checkDuplicities = arr => {
  arr = arr.filter((thing, index, self) =>
    index === self.findIndex((t) => (
      t.location[0] === thing.location[0] && t.location[1] === thing.location[1]
    ))
  )
  return arr
}

// var urls = require('./result.Country.json');
// console.log('before',urls.length)
// console.log('after',checkDuplicities(urls).length)
// console.log('base',require('./links.json').length);
